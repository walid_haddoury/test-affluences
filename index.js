const express = require('express');
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


let allNotes =  [];

function seeAllNotes (){
    return allNotes;
}

function addNote(newNote){
    const buffer = {...newNote};
    allNotes.push(buffer);
    return allNotes;
}

function removeNote(titleNote){
    allNotes = allNotes.filter(value => value.title !== titleNote);
    return allNotes;
}

function replaceNote(titleOfNote, newNote){
    const buffer = {...newNote};
    allNotes = allNotes.filter(value => value.title !== titleOfNote);
    allNotes.push(buffer);
}



app.get('/', function (req, res) {
    res.send('bonsoir')
});

app.post('/newNote', function (req, res) {
        const newNote = req.body;
        newNote.id = allNotes.length +1;
        return res.send(addNote(newNote));
});

app.delete('/deleteNote/:id', function (req, res) {
    try {
        const idNote = req.params.id;
        return res.send(removeNote(idNote));
    } catch (e) {
        res.status(500).send(e)
    }
});

app.put('/replaceNote/:id', function (req, res) {
    try {
        const idNote = req.params.id;
        const newNote = req.body;
        return res.send(replaceNote(idNote, newNote));
    } catch (e) {
        res.status(500).send(e)
    }
});

app.get('/allNotes', function (req, res) {
        res.send(seeAllNotes());
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});
